# Ansible Molecule Fedora

[![Docker Repository on Quay](https://quay.io/repository/maartenq/ansible-molecule-fedora/status "Docker Repository on Quay")](https://quay.io/repository/maartenq/ansible-molecule-fedora)

Dockerfile for building container image for use for Ansible Molecule testing on Fedora. Supplies systemd init and ansible sudo user.
